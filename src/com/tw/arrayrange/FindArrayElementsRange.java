package com.tw.arrayrange;

import java.util.Scanner;

public class FindArrayElementsRange {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter number of elements:");
        int numberOfElements= scanner.nextInt();

        System.out.println("Enter Array elements");
        int[] arrayElements=new int[numberOfElements];
        for(int index=0;index<numberOfElements;index++)
            arrayElements[index]= scanner.nextInt();

        int maxElement=Integer.MIN_VALUE,minElement=Integer.MAX_VALUE;
        for(int index=0;index<numberOfElements;index++){
            if(arrayElements[index]<minElement)
                minElement=arrayElements[index];
            else
                maxElement=arrayElements[index];
        }

        System.out.println("Range value:"+(maxElement-minElement));
    }
}
